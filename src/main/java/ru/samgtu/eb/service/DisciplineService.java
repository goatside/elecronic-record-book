package ru.samgtu.eb.service;

import org.springframework.stereotype.Service;
import ru.samgtu.eb.converter.DisciplineConverter;
import ru.samgtu.eb.dto.DisciplineDTO;
import ru.samgtu.eb.exceptions.NotFoundException;
import ru.samgtu.eb.models.Discipline;
import ru.samgtu.eb.repository.DisciplineRepo;

import java.util.List;

@Service
public class DisciplineService extends GenericService<Discipline, DisciplineDTO> {

    private final DisciplineRepo disciplineRepo;

    public DisciplineService(DisciplineRepo disciplineRepo) { this.disciplineRepo = disciplineRepo; }

    public List<Discipline> findAllByStudentId(Long studentId) {
        return disciplineRepo.findAllByStudentId(studentId);
    }
    @Override
    public Discipline createFromDTO(DisciplineDTO dtoObject) {
        return disciplineRepo.save(DisciplineConverter.toDomain(dtoObject));
    }

    @Override
    public Discipline getOne(Long objectID) throws NotFoundException {
        return disciplineRepo.findById(objectID).orElseThrow(
                () -> new NotFoundException("Discipline with id=" + objectID + " not found!")
        );
    }

    @Override
    public List<Discipline> getAll() { return disciplineRepo.findAll(); }

    @Override
    public Discipline updateFromDTO(DisciplineDTO dtoObject, Long objectID) throws NotFoundException {
        Discipline discipline = disciplineRepo.findById(objectID).orElseThrow(
                () -> new NotFoundException("Discipline with id=" + objectID + " not found!")
        );
        discipline.setExam(dtoObject.getExam());
        discipline.setTest(dtoObject.getTest());
        return discipline;
    }

    @Override
    public String delete(Long objectID) throws NotFoundException {
        disciplineRepo.findById(objectID).orElseThrow(
                () -> new NotFoundException("Discipline with id=" + objectID + " not found! ")
        );

        disciplineRepo.deleteById(objectID);

        return "Discipline with id= " + objectID + " successfully deleted!";
    }
}
