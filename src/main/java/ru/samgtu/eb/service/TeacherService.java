package ru.samgtu.eb.service;

import org.springframework.stereotype.Service;
import ru.samgtu.eb.dto.TeacherDTO;
import ru.samgtu.eb.exceptions.NotFoundException;
import ru.samgtu.eb.models.Teacher;
import ru.samgtu.eb.repository.GroupeRepo;
import ru.samgtu.eb.repository.TeacherRepo;

import java.util.List;

@Service
public class TeacherService extends GenericService<Teacher, TeacherDTO> {
    private final TeacherRepo teacherRepo;

    public TeacherService(TeacherRepo teacherRepo) {
        this.teacherRepo = teacherRepo;
    }

    @Override
    public Teacher createFromDTO(TeacherDTO dtoObject) {
        Teacher teacher = new Teacher();
        teacher.setId(dtoObject.getId());
        teacher.setCreatedBy(dtoObject.getCreatedBy());
        teacher.setCreatedWhen(dtoObject.getCreatedWhen());
        teacher.setName(dtoObject.getName());
        teacher.setSurname(dtoObject.getSurmane());
        teacher.setDiscipline(dtoObject.getDiscipline());
        teacher.setPatronymic(dtoObject.getPatronymic());
        return teacherRepo.save(teacher);
    }

    @Override
    public Teacher getOne(Long objectID) throws NotFoundException {
        return teacherRepo.findById(objectID).orElseThrow(
                () -> new NotFoundException("Teacher with id=" + objectID + "not found!")
        );
    }

    @Override
    public List<Teacher> getAll() {return teacherRepo.findAll(); }

    @Override
    public Teacher updateFromDTO(TeacherDTO dtoObject, Long objectID) throws NotFoundException {
        Teacher teacher = teacherRepo.findById(objectID).orElseThrow(
                () -> new NotFoundException("Teacher with id=" + objectID + "not found")
        );
        teacher.setId(dtoObject.getId());
        teacher.setCreatedWhen(dtoObject.getCreatedWhen());
        teacher.setName(dtoObject.getName());
        teacher.setCreatedBy(dtoObject.getCreatedBy());
        teacher.setDiscipline(dtoObject.getDiscipline());
        teacher.setSurname(dtoObject.getSurmane());
        teacher.setPatronymic(dtoObject.getPatronymic());
        return teacher;
    }

    @Override
    public String delete(Long objectID) throws NotFoundException {
        teacherRepo.findById(objectID).orElseThrow(
                () -> new org.webjars.NotFoundException("Teacher with id=" + objectID + "not found!")
        );
        teacherRepo.deleteById(objectID);
        return "Teacher with id=" + objectID + " successfully deleted!";
    }
}