package ru.samgtu.eb.service;

import ru.samgtu.eb.exceptions.NotFoundException;

import java.util.List;

public abstract class GenericService<T, N> {

    public abstract T createFromDTO(N dtoObject);

    public abstract T getOne(Long objectID) throws NotFoundException;

    public abstract List<T> getAll();

    public abstract T updateFromDTO(N dtoObject, Long objectID) throws NotFoundException;

    public abstract String delete(Long objectID) throws NotFoundException;

}