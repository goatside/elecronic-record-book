package ru.samgtu.eb.service;

import org.springframework.stereotype.Service;
import ru.samgtu.eb.dto.ElectronicRecordBookDTO;
import ru.samgtu.eb.exceptions.NotFoundException;
import ru.samgtu.eb.models.ElectronicRecordBook;
import ru.samgtu.eb.repository.ElectronicRecordBookRepo;

import java.util.List;

@Service
public class ElectronicRecordBookService extends GenericService<ElectronicRecordBook, ElectronicRecordBookDTO> {
    private final ElectronicRecordBookRepo electronicRecordBookRepo;

    public ElectronicRecordBookService(ElectronicRecordBookRepo electronicRecordBookRepo) { this.electronicRecordBookRepo = electronicRecordBookRepo; }

    @Override
    public ElectronicRecordBook createFromDTO(ElectronicRecordBookDTO dtoObject) {
        ElectronicRecordBook electronicRecordBook = new ElectronicRecordBook();
        electronicRecordBook.setId(dtoObject.getId());
        electronicRecordBook.setCreatedWhen(dtoObject.getCreatedWhen());
        electronicRecordBook.setCreatedBy(dtoObject.getCreatedBy());
        electronicRecordBook.setDiscipline(dtoObject.getDiscipline());
        electronicRecordBook.setGroup(dtoObject.getGroup());
        electronicRecordBook.setStudent(dtoObject.getStudent());
        electronicRecordBook.setData(dtoObject.getData());
        electronicRecordBook.setTeacher(dtoObject.getTeacher());
        electronicRecordBook.setElectronic_signature(dtoObject.getElectronicSignature());
        return electronicRecordBookRepo.save(electronicRecordBook);
    }

    @Override
    public ElectronicRecordBook getOne(Long objectID) throws NotFoundException {

        return electronicRecordBookRepo.findById(objectID).orElseThrow(
                () -> new NotFoundException("ElectronicRecordBook with id=" + objectID + " not found!")
        ) ;
    }

    @Override
    public List<ElectronicRecordBook> getAll() {
        return electronicRecordBookRepo.findAll();
    }

    @Override
    public ElectronicRecordBook updateFromDTO(ElectronicRecordBookDTO dtoObject, Long objectID) throws NotFoundException {
        ElectronicRecordBook electronicRecordBook = electronicRecordBookRepo.findById(objectID).orElseThrow(
                () -> new NotFoundException("ElectronicRecordBook with id= " + objectID + " not found!")
        );
        electronicRecordBook.setId(dtoObject.getId());
        electronicRecordBook.setCreatedWhen(dtoObject.getCreatedWhen());
        electronicRecordBook.setCreatedBy(dtoObject.getCreatedBy());
        electronicRecordBook.setDiscipline(dtoObject.getDiscipline());
        electronicRecordBook.setGroup(dtoObject.getGroup());
        electronicRecordBook.setStudent(dtoObject.getStudent());
        electronicRecordBook.setData(dtoObject.getData());
        electronicRecordBook.setTeacher(dtoObject.getTeacher());
        electronicRecordBook.setElectronic_signature(dtoObject.getElectronicSignature());
        return electronicRecordBook;
    }

    @Override
    public String delete(Long objectID) throws NotFoundException {
        electronicRecordBookRepo.findById(objectID).orElseThrow(
                () -> new NotFoundException("Electronic with id=" + objectID + " not found!")
        );
        return null;
    }
}
