package ru.samgtu.eb.service;

import org.springframework.stereotype.Service;
import ru.samgtu.eb.dto.GroupeDTO;
import ru.samgtu.eb.exceptions.NotFoundException;
import ru.samgtu.eb.models.Groupe;
import ru.samgtu.eb.repository.GroupeRepo;

import java.util.List;

@Service
public class GroupeService extends GenericService<Groupe, GroupeDTO> {
    private final GroupeRepo groupeRepo;

    public GroupeService(GroupeRepo groupeRepo) {
        this.groupeRepo = groupeRepo;
    }

    @Override
    public Groupe createFromDTO(GroupeDTO dtoObject) {
        Groupe groupe = new Groupe();
        groupe.setCreatedBy(dtoObject.getCreatedBy());
        groupe.setCreatedWhen(dtoObject.getCreatedWhen());
        groupe.setId(dtoObject.getId());
        groupe.setDirection(dtoObject.getDirection());
        groupe.setNumber(dtoObject.getNumber());
        return groupeRepo.save(groupe);
    }

    @Override
    public Groupe getOne(Long objectID) throws NotFoundException {
        return groupeRepo.findById(objectID).orElseThrow(
                () -> new NotFoundException("Groupe with id=" + objectID + "not found!")
        );
    }

    @Override
    public List<Groupe> getAll() {return groupeRepo.findAll(); }

    @Override
    public Groupe updateFromDTO(GroupeDTO dtoObject, Long objectID) throws NotFoundException {
        Groupe groupe = groupeRepo.findById(objectID).orElseThrow(
                () -> new org.webjars.NotFoundException("Groupe with id=" + objectID + "not found!")
        );
        groupe.setNumber(dtoObject.getNumber());
        groupe.setCreatedWhen(dtoObject.getCreatedWhen());
        groupe.setCreatedBy(dtoObject.getCreatedBy());
        groupe.setId(dtoObject.getId());
        groupe.setDirection(dtoObject.getDirection());
        return groupe;
    }

    @Override
    public String delete(Long objectID) throws NotFoundException {
        groupeRepo.findById(objectID).orElseThrow(
                () -> new NotFoundException("Groupe with id=" + objectID + "not found!")
        );
        groupeRepo.deleteById(objectID);
        return "Groupe wirh id=" + objectID + "successfully deleted!";
    }
}