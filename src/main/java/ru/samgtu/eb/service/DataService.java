package ru.samgtu.eb.service;

import org.springframework.stereotype.Service;
import ru.samgtu.eb.dto.DataDTO;
import ru.samgtu.eb.exceptions.NotFoundException;
import ru.samgtu.eb.models.Data;
import ru.samgtu.eb.repository.DataRepo;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class DataService extends GenericService<Data, DataDTO> {

    private final DataRepo dataRepo;

    public DataService(DataRepo dataRepo) {
        this.dataRepo = dataRepo;
    }

    @Override
    public Data createFromDTO(DataDTO dtoObject) {
        Data data = new Data();
        data.setDay(dtoObject.getDay());
        data.setMonth(dtoObject.getMonth());
        data.setYear(dtoObject.getYear());
        data.setCreatedBy(dtoObject.getCreatedBy());
        data.setCreatedWhen(LocalDateTime.now());
        return dataRepo.save(data);
    }

    @Override
    public Data getOne(Long objectID) throws NotFoundException {

        return dataRepo.findById(objectID).orElseThrow(
                () -> new NotFoundException("Data with id=" + objectID + " not found!")
        );
    }

    @Override
    public List<Data> getAll() {
        return dataRepo.findAll();
    }

    @Override
    public Data updateFromDTO(DataDTO dtoObject, Long objectID) throws NotFoundException {
        Data data = dataRepo.findById(objectID).orElseThrow(
                () -> new NotFoundException("Data with id=" + objectID + " not found!")
        );
        data.setDay(dtoObject.getDay());
        data.setMonth(dtoObject.getMonth());
        data.setYear(dtoObject.getYear());
        data.setCreatedBy(dtoObject.getCreatedBy());
        return data;
    }

    @Override
    public String delete(Long objectID) throws NotFoundException {
        dataRepo.findById(objectID).orElseThrow(
                () -> new NotFoundException("Data with id=" + objectID + " not found!")
        );

        dataRepo.deleteById(objectID);

        return "Data with id= " + objectID + " successfully deleted!";
    }
}
