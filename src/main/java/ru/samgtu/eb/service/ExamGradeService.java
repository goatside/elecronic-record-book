package ru.samgtu.eb.service;

import org.springframework.stereotype.Service;
import ru.samgtu.eb.dto.ExamGradeDTO;
import ru.samgtu.eb.exceptions.NotFoundException;
import ru.samgtu.eb.models.ExamGrade;
import ru.samgtu.eb.repository.ExamGradeRepo;

import java.util.List;

@Service
public class ExamGradeService extends GenericService<ExamGrade, ExamGradeDTO> {

    private final ExamGradeRepo examGradeRepo;

    public ExamGradeService(ExamGradeRepo examGradeRepo) {
        this.examGradeRepo = examGradeRepo;
    }

    @Override
    public ExamGrade createFromDTO(ExamGradeDTO dtoObject) {
        ExamGrade examGrade = new ExamGrade();
        examGrade.setId(dtoObject.getId());
        examGrade.setCreatedWhen(dtoObject.getCreatedWhen());
        examGrade.setCreatedBy(dtoObject.getCreatedBy());
        examGrade.setGrade(dtoObject.getGrade());
        return examGradeRepo.save(examGrade);
    }

    @Override
    public ExamGrade getOne(Long objectID) throws NotFoundException {
        return examGradeRepo.findById(objectID).orElseThrow(
                () -> new NotFoundException("ExamGrade with id=" + objectID + "not found!")
        );
    }

    @Override
    public List<ExamGrade> getAll() {
        return examGradeRepo.findAll();
    }


    @Override
    public ExamGrade updateFromDTO(ExamGradeDTO dtoObject, Long objectID) throws NotFoundException {
        ExamGrade examGrade = examGradeRepo.findById(objectID).orElseThrow(
                () -> new NotFoundException("ExamGrade with id=" + objectID + "not found!")
        );
        examGrade.setGrade(dtoObject.getGrade());
        examGrade.setId(dtoObject.getId());
        examGrade.setCreatedBy(dtoObject.getCreatedBy());
        examGrade.setCreatedWhen(dtoObject.getCreatedWhen());
        return examGrade;
    }

    @Override
    public String delete(Long objectID) throws NotFoundException {
        examGradeRepo.findById(objectID).orElseThrow(
                () -> new org.webjars.NotFoundException("ExamGrade with id=" + objectID + "not found!")
        );
        examGradeRepo.deleteById(objectID);
        return "ExamGrade with id=" + objectID + "successfully deleted!";
    }
}
