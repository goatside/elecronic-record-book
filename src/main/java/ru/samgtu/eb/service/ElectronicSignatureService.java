package ru.samgtu.eb.service;

import org.springframework.stereotype.Service;
import ru.samgtu.eb.dto.ElectronicSignatureDTO;
import ru.samgtu.eb.exceptions.NotFoundException;
import ru.samgtu.eb.models.ElectronicSignature;
import ru.samgtu.eb.repository.ElectronicSignatureRepo;

import java.util.List;

@Service
public class ElectronicSignatureService extends GenericService<ElectronicSignature, ElectronicSignatureDTO> {

    private final ElectronicSignatureRepo electronicSignatureRepo;

    public ElectronicSignatureService(ElectronicSignatureRepo electronicSignatureRepo) {
        this.electronicSignatureRepo = electronicSignatureRepo;
    }


    @Override
    public ElectronicSignature createFromDTO(ElectronicSignatureDTO dtoObject) {
        ElectronicSignature electronicSignature = new ElectronicSignature();
        electronicSignature.setId(dtoObject.getId());
        electronicSignature.setCreatedWhen(dtoObject.getCreatedWhen());
        electronicSignature.setCreatedBy(dtoObject.getCreatedBy());
        electronicSignature.setElectronic_signature(dtoObject.getSignature());
        return electronicSignatureRepo.save(electronicSignature);
    }

    @Override
    public ElectronicSignature getOne(Long objectID) throws NotFoundException {
        return electronicSignatureRepo.findById(objectID).orElseThrow(
                () -> new NotFoundException("ElectronicSignature with id=" + objectID + "not found!")
        );
    }

    @Override
    public List<ElectronicSignature> getAll() {
        return electronicSignatureRepo.findAll();
    }

    @Override
    public ElectronicSignature updateFromDTO(ElectronicSignatureDTO dtoObject, Long objectID) throws NotFoundException {
        ElectronicSignature electronicSignature = electronicSignatureRepo.findById(objectID).orElseThrow(
                () -> new NotFoundException("ElectronicSignature with id=" + objectID + "not found!")
        );
        electronicSignature.setId(dtoObject.getId());
        electronicSignature.setCreatedWhen(dtoObject.getCreatedWhen());
        electronicSignature.setCreatedBy(dtoObject.getCreatedBy());
        electronicSignature.setElectronic_signature(dtoObject.getSignature());
        return electronicSignature;
    }

    @Override
    public String delete(Long objectID) throws NotFoundException {
        electronicSignatureRepo.findById(objectID).orElseThrow(
                () -> new NotFoundException("ElectronicSignature with id=" + objectID + "not found!")
        );
        electronicSignatureRepo.deleteById(objectID);
        return "ElectronicSignature with id= " + objectID + " succcessfully deleted";
    }
}
