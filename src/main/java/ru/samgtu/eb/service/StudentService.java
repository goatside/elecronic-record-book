package ru.samgtu.eb.service;

import org.springframework.stereotype.Service;
import ru.samgtu.eb.converter.DisciplineConverter;
import ru.samgtu.eb.dto.StudentDTO;
import ru.samgtu.eb.exceptions.NotFoundException;
import ru.samgtu.eb.models.Discipline;
import ru.samgtu.eb.models.Student;
import ru.samgtu.eb.repository.StudentRepo;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentService extends GenericService<Student, StudentDTO> {
    private final StudentRepo studentRepo;

    public StudentService(StudentRepo studentRepo) {
        this.studentRepo = studentRepo;
    }

    @Override
    public Student createFromDTO(StudentDTO dtoObject) {
        Student student = new Student();
        student.setId(dtoObject.getId());
        student.setCreatedBy(dtoObject.getCreatedBy());
        student.setCreatedWhen(dtoObject.getCreatedWhen());
        student.setName(dtoObject.getName());
        student.setCourse(dtoObject.getCourse());
        student.setPatronymic(dtoObject.getPatronymic());
        student.setSurname(dtoObject.getSurmane());
        student.setDiscipline(dtoObject.getDiscipline().stream().map(DisciplineConverter::toDomain).collect(Collectors.toSet()));
        return studentRepo.save(student);
    }

    @Override
    public Student getOne(Long objectID) throws NotFoundException {
        return studentRepo.findById(objectID).orElseThrow(
                () -> new org.webjars.NotFoundException("Student with id=" + objectID + "not found!")
        );
    }

    @Override
    public List<Student> getAll() { return studentRepo.findAll();
    }

    @Override
    public Student updateFromDTO(StudentDTO dtoObject, Long objectID) throws NotFoundException {
        Student student = studentRepo.findById(objectID).orElseThrow(
                () -> new org.webjars.NotFoundException("Student with id=" + objectID + "not found!")
        );
        student.setId(dtoObject.getId());
        student.setCreatedBy(dtoObject.getCreatedBy());
        student.setCreatedWhen(dtoObject.getCreatedWhen());
        student.setCourse(dtoObject.getCourse());
        student.setName(dtoObject.getName());
        student.setPatronymic(dtoObject.getPatronymic());
        student.setSurname(dtoObject.getSurmane());
        return student;
    }

    @Override
    public String delete(Long objectID) throws NotFoundException {
        studentRepo.findById(objectID).orElseThrow(
                () -> new org.webjars.NotFoundException("Student with id=" + objectID + "not found!")
        );
        studentRepo.deleteById(objectID);
        return "Student with id=" + objectID + "successfully deleted!";
    }
}
