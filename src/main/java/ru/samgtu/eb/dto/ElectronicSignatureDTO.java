package ru.samgtu.eb.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.samgtu.eb.models.ElectronicSignature;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@ToString

public class ElectronicSignatureDTO {

    private Long id;
    private LocalDateTime createdWhen;
    private String createdBy;
    private String signature;
    private Set<TeacherDTO> teacher = new HashSet<>();

    public ElectronicSignatureDTO(ElectronicSignature electronicSignature){
        this.id = electronicSignature.getId();
        this.createdWhen = electronicSignature.getCreatedWhen();
        this.createdBy = electronicSignature.getCreatedBy();
        this.signature = electronicSignature.getElectronic_signature();
        //this.teacher = electronicSignature.getTeacher().stream().map(TeacherDTO::new).collect(Collectors.toSet());;
    }
}
