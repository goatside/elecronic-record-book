package ru.samgtu.eb.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.samgtu.eb.models.Discipline;
import ru.samgtu.eb.models.Teacher;

import javax.xml.transform.sax.SAXResult;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@ToString

public class TeacherDTO {

    private Long id;
    private LocalDateTime createdWhen;
    private String createdBy;
    private String name;
    private String surmane;
    private String patronymic;
    private Set<Discipline> discipline;


    public TeacherDTO(Teacher teacher) {
        this.id = teacher.getId();
        this.createdWhen = teacher.getCreatedWhen();
        this.createdBy = teacher.getCreatedBy();
        this.name = teacher.getName();
        this.surmane = teacher.getSurname();
        this.patronymic = teacher.getPatronymic();
        this.discipline = teacher.getDiscipline();
    }
}