package ru.samgtu.eb.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.samgtu.eb.models.Discipline;
import ru.samgtu.eb.models.ElectronicRecordBook;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@ToString

public class ElectronicRecordBookDTO {

    private Long id;
    private LocalDateTime createdWhen;
    private String createdBy;
    private String discipline;
    private String group;
    private String student;
    private String data;
    private String teacher;
    private String electronicSignature;

    public ElectronicRecordBookDTO(ElectronicRecordBook electronicRecordBook){
        this.id = electronicRecordBook.getId();
        this.createdWhen = electronicRecordBook.getCreatedWhen();
        this.createdBy = electronicRecordBook.getCreatedBy();
        this.group = electronicRecordBook.getGroup();
        this.student = electronicRecordBook.getStudent();
        this.data = electronicRecordBook.getData();
        this.teacher = electronicRecordBook.getTeacher();
        this.electronicSignature = electronicRecordBook.getElectronic_signature();
    }
}
