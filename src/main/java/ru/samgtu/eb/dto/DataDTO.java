package ru.samgtu.eb.dto;

import ru.samgtu.eb.models.Data;

import lombok.*;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@ToString

public class DataDTO {

    private Long id;
    private LocalDateTime createdWhen;
    private String createdBy;
    private String day;
    private String month;
    private String year;
    private Set<ExamGradeDTO> examGrade = new HashSet<>();

    public DataDTO(Data data){
        this.id = data.getId();
        this.createdWhen = data.getCreatedWhen();
        this.createdBy = data.getCreatedBy();
        this.day = data.getDay();
        this.month = data.getMonth();
        this.year = data.getYear();
        this.examGrade = data.getExamGrade().stream().map(ExamGradeDTO::new).collect(Collectors.toSet());
    }
}
