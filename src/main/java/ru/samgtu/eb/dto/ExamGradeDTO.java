package ru.samgtu.eb.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.samgtu.eb.models.ExamGrade;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class ExamGradeDTO {

    private Long id;
    private LocalDateTime createdWhen;
    private String createdBy;
    private String grade;
    private Set<DataDTO> data = new HashSet<>();

    public ExamGradeDTO(ExamGrade examGrade){
        this.id = examGrade.getId();
        this.createdWhen = examGrade.getCreatedWhen();
        this.createdBy = examGrade.getCreatedBy();
        this.grade = examGrade.getGrade();
        this.data = examGrade.getData().stream().map(DataDTO::new).collect(Collectors.toSet());;
    }
}

