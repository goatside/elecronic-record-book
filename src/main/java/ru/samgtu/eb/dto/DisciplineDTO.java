package ru.samgtu.eb.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.samgtu.eb.models.Discipline;
import ru.samgtu.eb.models.Student;
import ru.samgtu.eb.models.Teacher;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@ToString

public class DisciplineDTO {

    private Long id;
    private LocalDateTime createdWhen;
    private String createdBy;
    private String exam;
    private String test;
    private Teacher teacher;
    private Long teacherId;
    private Set<Student> student;

    public DisciplineDTO(Discipline discipline) {
        this.id = discipline.getId();
        this.createdWhen = discipline.getCreatedWhen();
        this.createdBy = discipline.getCreatedBy();
        this.exam = discipline.getExam();
        this.test = discipline.getTest();
        this.teacher = discipline.getTeacher();
        this.student = discipline.getStudent();
    }
}
