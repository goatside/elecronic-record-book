package ru.samgtu.eb.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.samgtu.eb.models.Groupe;
import ru.samgtu.eb.models.Student;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@ToString

public class StudentDTO {

    private Long id;
    private LocalDateTime createdWhen;
    private String createdBy;
    private String name;
    private String surmane;
    private String patronymic;
    private String course;
    private Set<DisciplineDTO> discipline = new HashSet<>();
    private Set<GroupeDTO> groupe = new HashSet<>();

    public StudentDTO(Student student){
        this.id = student.getId();
        this.createdWhen = student.getCreatedWhen();
        this.createdBy = student.getCreatedBy();
        this.name = student.getName();
        this.surmane = student.getSurname();
        this.patronymic = student.getPatronymic();
        this.course = student.getCourse();
        this.discipline = student.getDiscipline().stream().map(DisciplineDTO::new).collect(Collectors.toSet());
        this.groupe = student.getGroupe().stream().map(GroupeDTO::new).collect(Collectors.toSet());
        //this.groupe = student.getGroupe();
    }
}
