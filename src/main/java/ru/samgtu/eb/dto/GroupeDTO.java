package ru.samgtu.eb.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.samgtu.eb.models.Groupe;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@ToString

public class GroupeDTO {

    private Long id;
    private LocalDateTime createdWhen;
    private String createdBy;
    private String number;
    private String direction;
    private Set<StudentDTO> student = new HashSet<>();

    public GroupeDTO(Groupe groupe){
        this.id = groupe.getId();
        this.createdWhen = groupe.getCreatedWhen();
        this.createdBy = groupe.getCreatedBy();
        this.number = groupe.getNumber();
        this.direction = groupe.getDirection();
        this.student = groupe.getStudent().stream().map(StudentDTO::new).collect(Collectors.toSet());;
    }
}
