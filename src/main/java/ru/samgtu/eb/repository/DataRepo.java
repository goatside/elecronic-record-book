package ru.samgtu.eb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.samgtu.eb.models.Data;

@Repository
public interface DataRepo extends JpaRepository<Data, Long> {
}
