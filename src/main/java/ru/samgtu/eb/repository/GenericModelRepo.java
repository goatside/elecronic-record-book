package ru.samgtu.eb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.samgtu.eb.models.GenericModel;

@Repository
public interface GenericModelRepo extends JpaRepository<GenericModel, Long> {
}
