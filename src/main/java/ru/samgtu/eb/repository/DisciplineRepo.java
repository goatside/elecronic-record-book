package ru.samgtu.eb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.samgtu.eb.models.Discipline;

import java.util.List;

@Repository
public interface DisciplineRepo extends JpaRepository<Discipline, Long> {
    List<Discipline> findAllByStudentId(Long studentId);
}
