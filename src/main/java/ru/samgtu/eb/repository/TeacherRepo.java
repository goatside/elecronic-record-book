package ru.samgtu.eb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.samgtu.eb.models.Teacher;

@Repository
public interface TeacherRepo extends JpaRepository<Teacher, Long> {
}
