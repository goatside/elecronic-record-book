package ru.samgtu.eb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.samgtu.eb.models.ElectronicRecordBook;

@Repository
public interface ElectronicRecordBookRepo extends JpaRepository<ElectronicRecordBook, Long> {
}
