package ru.samgtu.eb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.samgtu.eb.models.ExamGrade;

@Repository
public interface ExamGradeRepo extends JpaRepository<ExamGrade, Long> {
}
