package ru.samgtu.eb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.samgtu.eb.models.Groupe;

@Repository
public interface GroupeRepo extends JpaRepository<Groupe, Long> {
}
