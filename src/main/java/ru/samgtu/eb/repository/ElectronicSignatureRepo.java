package ru.samgtu.eb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.samgtu.eb.models.ElectronicSignature;

@Repository
public interface ElectronicSignatureRepo extends JpaRepository<ElectronicSignature, Long> {
}
