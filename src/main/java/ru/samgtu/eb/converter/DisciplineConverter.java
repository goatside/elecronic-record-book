package ru.samgtu.eb.converter;

import ru.samgtu.eb.dto.DisciplineDTO;
import ru.samgtu.eb.models.Discipline;

public class DisciplineConverter {
    public static Discipline toDomain(DisciplineDTO disciplineDTO) {
        Discipline discipline = new Discipline();
        discipline.setExam(disciplineDTO.getExam());
        discipline.setStudent(disciplineDTO.getStudent());
        discipline.setTeacher(disciplineDTO.getTeacher());
        discipline.setCreatedBy(disciplineDTO.getCreatedBy());
        discipline.setTest(disciplineDTO.getTest());
        discipline.setId(disciplineDTO.getId());
        return discipline;
    }
}
