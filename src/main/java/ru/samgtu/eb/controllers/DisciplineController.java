package ru.samgtu.eb.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.samgtu.eb.dto.DisciplineDTO;
import ru.samgtu.eb.exceptions.NotFoundException;
import ru.samgtu.eb.service.DisciplineService;

@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping(value = "/api/discipline", consumes = MediaType.ALL_VALUE)
@Controller
public class DisciplineController {

    private DisciplineService disciplineService;

    public DisciplineController(DisciplineService disciplineService) {
        this.disciplineService = disciplineService;
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createDiscipline(@RequestBody DisciplineDTO dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(disciplineService.createFromDTO(dto));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getOne(@PathVariable("id") Long objectID) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(disciplineService.getOne(objectID));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Data not found!");
        }
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllDiscipline() {
        return ResponseEntity.status(HttpStatus.OK).body(disciplineService.getAll());
    }

    @RequestMapping(value = "/student/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllDisciplineByStudentId(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(disciplineService.findAllByStudentId(id));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateDiscipline(@PathVariable("id") Long objectID,
                                        @RequestBody DisciplineDTO dto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(disciplineService.updateFromDTO(dto, objectID));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Discipline not found!");
        }
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteDiscipline(@RequestParam("id") Long objectID) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(disciplineService.delete(objectID));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Discipline not found");
        }
    }

}