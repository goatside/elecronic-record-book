package ru.samgtu.eb.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.samgtu.eb.dto.GroupeDTO;
import ru.samgtu.eb.exceptions.NotFoundException;
import ru.samgtu.eb.service.GroupeService;

@RestController
@RequestMapping(value = "/api/data", consumes = MediaType.ALL_VALUE)
public class GroupeController {
    private final GroupeService groupeService;

    public GroupeController(GroupeService dataService, GroupeService groupeService) {
        this.groupeService = groupeService;
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createGroupe(@RequestBody GroupeDTO dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(groupeService.createFromDTO(dto));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getOne(@PathVariable("id") Long objectID) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(groupeService.getOne(objectID));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Groupe not found!");
        }
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllGroupe() {
        return ResponseEntity.status(HttpStatus.OK).body(groupeService.getAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateGroupe(@PathVariable("id") Long objectID,
                                        @RequestBody GroupeDTO dto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(groupeService.updateFromDTO(dto, objectID));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Groupe not found!");
        }
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteGroupe(@RequestParam("id") Long objectID) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(groupeService.delete(objectID));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Groupe not found!");
        }
    }

}
