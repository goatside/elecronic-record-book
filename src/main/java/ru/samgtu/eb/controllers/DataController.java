package ru.samgtu.eb.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.samgtu.eb.dto.DataDTO;
import ru.samgtu.eb.exceptions.NotFoundException;
import ru.samgtu.eb.service.DataService;
public class DataController {
    private final DataService dataService;

    public DataController(DataService dataService) {
        this.dataService = dataService;
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createData(@RequestBody DataDTO dto) {
       return ResponseEntity.status(HttpStatus.CREATED).body(dataService.createFromDTO(dto));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getOne(@PathVariable("id") Long objectID) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(dataService.getOne(objectID));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Data not found!");
        }
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllData() {
        return ResponseEntity.status(HttpStatus.OK).body(dataService.getAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateData(@PathVariable("id") Long objectID,
                                        @RequestBody DataDTO dto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(dataService.updateFromDTO(dto, objectID));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Data not found!");
        }
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteData(@RequestParam("id") Long objectID) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(dataService.delete(objectID));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Data not found!");
        }
    }

}
