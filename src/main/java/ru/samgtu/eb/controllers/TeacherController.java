package ru.samgtu.eb.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.samgtu.eb.dto.TeacherDTO;
import ru.samgtu.eb.exceptions.NotFoundException;
import ru.samgtu.eb.models.Teacher;
import ru.samgtu.eb.service.TeacherService;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping(value = "/api/teacher", consumes = MediaType.ALL_VALUE)
public class TeacherController {
    private final TeacherService teacherService;

    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createTeacher(@RequestBody TeacherDTO dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(teacherService.createFromDTO(dto));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getOne(@PathVariable("id") Long objectID) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(teacherService.getOne(objectID));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Teacher not found!");
        }
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllTeacher() {
        return ResponseEntity.status(HttpStatus.OK).body(teacherService.getAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateTeacher(@PathVariable("id") Long objectID,
                                        @RequestBody TeacherDTO dto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(teacherService.updateFromDTO(dto, objectID));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Teacher not found!");
        }
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteTeacher(@RequestParam("id") Long objectID) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(teacherService.delete(objectID));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Teacher not found!");
        }
    }

}