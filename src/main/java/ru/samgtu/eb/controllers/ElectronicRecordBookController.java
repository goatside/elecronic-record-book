package ru.samgtu.eb.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.samgtu.eb.dto.ElectronicRecordBookDTO;
import ru.samgtu.eb.exceptions.NotFoundException;
import ru.samgtu.eb.service.ElectronicRecordBookService;

public class ElectronicRecordBookController {
    private final ElectronicRecordBookService electronicRecordBookService;

    public ElectronicRecordBookController(ElectronicRecordBookService electronicRecordBookService) {
        this.electronicRecordBookService = electronicRecordBookService;
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createElectronicRecordBook(@RequestBody ElectronicRecordBookDTO dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(electronicRecordBookService.createFromDTO(dto));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getOne(@PathVariable("id") Long objectID) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(electronicRecordBookService.getOne(objectID));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("ElectronicRecordBook not found!");
        }
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllElectronicRecordBook() {
        return ResponseEntity.status(HttpStatus.OK).body(electronicRecordBookService.getAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateElectronicRecordBook(@PathVariable("id") Long objectID,
                                        @RequestBody ElectronicRecordBookDTO dto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(electronicRecordBookService.updateFromDTO(dto, objectID));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("ElectronicRecordBook not found!");
        }
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteElectronicRecordBook(@RequestParam("id") Long objectID) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(electronicRecordBookService.delete(objectID));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("ElectronicRecordBook not found!");
        }
    }

}