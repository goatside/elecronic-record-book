package ru.samgtu.eb.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.samgtu.eb.dto.ElectronicSignatureDTO;
import ru.samgtu.eb.exceptions.NotFoundException;
import ru.samgtu.eb.service.ElectronicSignatureService;

public class ElectronicSignatureController {
    private final ElectronicSignatureService electronicSignatureService;

    public ElectronicSignatureController(ElectronicSignatureService electronicSignatureService) {
        this.electronicSignatureService = electronicSignatureService;
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createElectronicSignature(@RequestBody ElectronicSignatureDTO dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(electronicSignatureService.createFromDTO(dto));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getOne(@PathVariable("id") Long objectID) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(electronicSignatureService.getOne(objectID));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("ElectronicSignature not found!");
        }
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllElectronicSignature() {
        return ResponseEntity.status(HttpStatus.OK).body(electronicSignatureService.getAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateElectronicSignature(@PathVariable("id") Long objectID,
                                        @RequestBody ElectronicSignatureDTO dto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(electronicSignatureService.updateFromDTO(dto, objectID));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("ElectronicSignature not found!");
        }
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteElectronicSignature(@RequestParam("id") Long objectID) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(electronicSignatureService.delete(objectID));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("ElectronicSignature not found!");
        }
    }

}