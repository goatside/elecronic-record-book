package ru.samgtu.eb.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.samgtu.eb.dto.ExamGradeDTO;
import ru.samgtu.eb.exceptions.NotFoundException;
import ru.samgtu.eb.service.ExamGradeService;
import ru.samgtu.eb.service.ExamGradeService;

@RestController
@RequestMapping(value = "/api/ExamGrade", consumes = MediaType.ALL_VALUE)
public class ExamGradeController {
    private final ExamGradeService examGradeService;

    public ExamGradeController(ExamGradeService examGradeService) {
        this.examGradeService = examGradeService;
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createExamGrade(@RequestBody ExamGradeDTO dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(examGradeService.createFromDTO(new ExamGradeDTO()));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getOneExamGrade(@PathVariable("id") Long objectID) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(examGradeService.getOne(objectID));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("ExamGrade not found!");
        }
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllExamGrade() {
        return ResponseEntity.status(HttpStatus.OK).body(examGradeService.getAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateExamGrade(@PathVariable("id") Long objectID,
                                        @RequestBody ExamGradeDTO dto) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(examGradeService.updateFromDTO(dto, objectID));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("ExamGrade not found!");
        }
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteExamGrade(@RequestParam("id") Long objectID) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(examGradeService.delete(objectID));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("ExamGrade not found!");
        }
    }

}

