package ru.samgtu.eb.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "electronic_signature")
@NoArgsConstructor
@Getter
@Setter
@ToString
@SequenceGenerator(name = "default_gen", sequenceName = "electronic_signature_seq", allocationSize = 1)
public class ElectronicSignature
        extends GenericModel {
    @Column(name = "electronic_signature")
    private String electronic_signature;
//    @JsonIgnore
//    @ManyToMany(mappedBy = "electronic_signature", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
//    @ToString.Exclude
//    private Set<Teacher> teacher = new HashSet<>();

}
