package ru.samgtu.eb.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "electonic_record_book")
@NoArgsConstructor
@Getter
@Setter
@ToString
@SequenceGenerator(name = "default_gen", sequenceName = "electonic_record_book_seq", allocationSize = 1)

public class ElectronicRecordBook
        extends GenericModel {

    @Column(name = "discipline")
    private String discipline;

    @Column(name = "data")
    private String data;

    @Column(name = "grp")
    private String group;

    @Column(name = "course")
    private String student;

    @Column(name = "teacher")
    private String teacher;

    @Column(name = "electronic_signature")
    private String electronic_signature;

}
