package ru.samgtu.eb.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "student")
@NoArgsConstructor
@Getter
@Setter
@ToString
@SequenceGenerator(name = "default_gen", sequenceName = "student_seq", allocationSize = 1)
public class Student
        extends GenericModel{

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "patronymic")
    private String patronymic;

    @Column(name = "course")
    private String course;
    @ManyToMany
    @JoinTable(name="student_discipline",
            joinColumns = @JoinColumn(name="student_id", referencedColumnName="id"),
            inverseJoinColumns = @JoinColumn(name="discipline_id", referencedColumnName="id")
    )
    @ToString.Exclude
    private Set<Discipline> discipline = new HashSet<>();
    @JsonIgnore
    @ManyToMany(mappedBy = "student", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @ToString.Exclude
    private Set<Groupe> groupe = new HashSet<>();
}
