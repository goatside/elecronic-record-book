package ru.samgtu.eb.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "exam_grade")
@NoArgsConstructor
@Getter
@Setter
@ToString
@SequenceGenerator(name = "default_gen", sequenceName = "exam_grade_seq", allocationSize = 1)
public class ExamGrade
        extends GenericModel{

    @Column(name = "grade")
    private String grade;
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinTable(name = "examgrade_data",
            joinColumns = @JoinColumn(name = "examgrade_id"), foreignKey = @ForeignKey(name = "FK_EXAMGRADE_DATA"),
            inverseJoinColumns = @JoinColumn(name = "data_id"), inverseForeignKey = @ForeignKey(name = "FK_DATA_EXAMGRADE"))
    @ToString.Exclude
    @JsonIgnore
    private Set<Data> data = new HashSet<>();
}
