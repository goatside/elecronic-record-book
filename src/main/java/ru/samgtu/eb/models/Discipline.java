package ru.samgtu.eb.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "discipline")
@NoArgsConstructor
@Getter
@Setter
@ToString
@SequenceGenerator(name = "default_gen", sequenceName = "discipline_seq", allocationSize = 1)
public class Discipline
        extends GenericModel{

    @Column(name = "exam")
    private String exam;

    @Column(name = "test")
    private String test;

    //подумать какой нужен fetchType и cascade
    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "teacher_id_fk")
    @ToString.Exclude
    private Teacher teacher;
    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "discipline")
    @ToString.Exclude
    private Set<Student> student = new HashSet<>();
}
