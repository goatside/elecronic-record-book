package ru.samgtu.eb.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "teacher")
@NoArgsConstructor
@Getter
@Setter
@ToString
@SequenceGenerator(name = "default_gen", sequenceName = "teacher_seq", allocationSize = 1)
public class Teacher
        extends GenericModel{

    @Column(name = "surname")
    private String surname;

    @Column(name = "name")
    private String name;

    @Column(name = "patronymic")
    private String patronymic;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "teacher")
    @ToString.Exclude
    private Set<Discipline> discipline = new HashSet<>();
}
