package ru.samgtu.eb.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "data")
@NoArgsConstructor
@Getter
@Setter
@ToString
@SequenceGenerator(name = "default_gen", sequenceName = "date_seq", allocationSize = 1)
public class Data
        extends GenericModel{

    @Column(name = "day")
    private String day;

    @Column(name = "month")
    private String month;

    @Column(name = "year")
    private String year;
    @JsonIgnore
    @ManyToMany(mappedBy = "data", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @ToString.Exclude
    private Set<ExamGrade> examGrade = new HashSet<>();
}
