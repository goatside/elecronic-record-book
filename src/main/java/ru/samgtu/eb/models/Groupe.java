package ru.samgtu.eb.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "groupe")
@NoArgsConstructor
@Getter
@Setter
@ToString
@SequenceGenerator(name = "default_gen", sequenceName = "groupe_seq", allocationSize = 1)
public class Groupe
        extends GenericModel{

    @Column(name = "number")
    private String number;

    @Column(name = "direction")
    private String direction;
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinTable(name = "groupe_student",
            joinColumns = @JoinColumn(name = "groupe_id"), foreignKey = @ForeignKey(name = "FK_GROUPE_STUDENT"),
            inverseJoinColumns = @JoinColumn(name = "student_id"), inverseForeignKey = @ForeignKey(name = "FK_STUDENT_GROUPE"))
    @ToString.Exclude
    @JsonIgnore
    private Set<Student> student = new HashSet<>();
}
