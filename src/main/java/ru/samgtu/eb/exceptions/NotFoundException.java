package ru.samgtu.eb.exceptions;

public class NotFoundException extends Exception {

    public NotFoundException(String message){
        super(message);
    };
}
